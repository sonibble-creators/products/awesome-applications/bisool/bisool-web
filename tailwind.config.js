const plugin = require('tailwindcss/plugin')
const tailwindForms = require('@tailwindcss/forms')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    fontFamily: {
      poppins: ['Poppins', 'sans-serif'],
      sans: ['DM Sans', 'sans-serif'],
    },
    colors: {
      black: '#000000',
      dark: '#262B3E',
      light: '#F5F9FD',
      white: '#FFFFFF',
      gray: {
        lighter: '#F8F9FF',
        light: '#EFF2FC',
        normal: '#DDE1ED',
        dark: '#8F97B0',
        darker: '#697390',
      },
      red: '#EA7B7B',
      green: '#38C581',
      orange: '#FFAB5E',
      blue: '#4148FF',
    },
    screens: {
      tablet: '640px',
      desktop: '1024px',
    },
    fontSize: {
      'display 1': '5.375rem',
      'heading-1': '4.5rem',
      'heading-2': '3.25rem',
      'heading-3': '2.5rem',
      'heading-4': '1.75rem',
      'heading-5': '1.5rem',
      'heading-6': '1.25rem',
      'subtitle-1': '1.125rem',
      'subtitle-2': '1.0625rem',
      'body-1': '1.0625rem',
      'body-2': '1rem',
      button: '1rem',
      labels: '.875rem',
    },
    extend: {},
  },
  plugins: [
    tailwindForms,
    plugin(function ({ addUtilities, matchUtilities, theme }) {
      addUtilities({
        '.no-scrollbar': {
          '-ms-overflow-style': 'none',
          'scrollbar-width': 'none',
        },
        '.no-scrollbar::-webkit-scrollbar': {
          display: 'none',
        },
      }),
        matchUtilities(
          {
            'text-stroke': (value) => ({
              '-webkit-text-stroke-width': `${value}px`,
            }),
          },
          { values: theme('strokeWidth') }
        )
      matchUtilities(
        {
          'text-stroke': (value) => ({
            '-webkit-text-stroke-color': value,
          }),
        },
        { values: theme('colors') }
      )
    }),
  ],
}
