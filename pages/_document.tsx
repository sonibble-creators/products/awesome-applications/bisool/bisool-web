import { FunctionComponent } from 'react'
import { Head, Html, Main, NextScript } from 'next/document'

/**
 * # BisoolDocument
 *
 * the custom document for bisool
 * contain some configuration and import
 *
 * @returns JSX.Element
 */
const BisoolDocument: FunctionComponent = (): JSX.Element => {
  return (
    <Html>
      <Head>
        {/* import some source from  */}
        {/* google fonts */}
        <link rel='preconnect' href='https://fonts.googleapis.com' />
        <link
          rel='preconnect'
          href='https://fonts.gstatic.com'
          crossOrigin=''
        />
        <link
          href='https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;1,400;1,500&family=Poppins:ital,wght@0,400;0,500;1,400;1,500&display=swap'
          rel='stylesheet'
        />

        {/* flaticon intraface icons */}
        <link
          rel='stylesheet'
          href='https://cdn-uicons.flaticon.com/uicons-regular-rounded/css/uicons-regular-rounded.css'
        />
        <link
          rel='stylesheet'
          href='https://cdn-uicons.flaticon.com/uicons-bold-rounded/css/uicons-bold-rounded.css'
        />
      </Head>
      <body className='font-sans text-body-1 text-black leading-normal dark:text-white bg-light dark:bg-dark selection:bg-dark selection:text-white selection:dark:bg-light selection:dark:text-black'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}

export default BisoolDocument
