import type { NextPage } from 'next'

const Home: NextPage = () => {
  return <div className='text-heading-1 font-medium'>Hello from</div>
}

export default Home
