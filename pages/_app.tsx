import { NextPage } from 'next'
import type { AppProps } from 'next/app'
import { ReactElement, ReactNode } from 'react'
import '@styles/main.css'

export type NextPageWithlayout<P = {}, IP = P> = NextPage<P, IP> & {
  layout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithlayout
}

/**
 * # BisoolApp
 *
 * the app that contain all about the pages
 * component and props
 *
 * @params data passed to the component
 * @returns JSX.Element | ReactNode | ReactElement
 */
function BisoolApp({
  Component,
  pageProps,
}: AppPropsWithLayout): JSX.Element | ReactNode | ReactElement {
  // get the current layout in each pages
  const layout = Component.layout ?? ((page) => page)

  return layout(<Component {...pageProps} />)
}

export default BisoolApp
